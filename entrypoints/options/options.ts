import './style.scss';

/**
 * Displays an alert message on the page and optionally hides it after a certain duration.
 * @param {string} msg - The message to display.
 * @param {string} type - The type of alert ('alert-success', 'alert-info', 'alert-warning', 'alert-danger').
 * @param {number} [duration=1000] - The duration in milliseconds before auto-hiding the alert (only for success alerts).
 */
const flashAlert = (msg: string, type: string, duration: number = 1000) => {
  const status = document.getElementById('status');
  const alertClasses = ['alert', type];

  // Set classes and text content
  status.className = alertClasses.join(' ');
  status.textContent = msg;
  status.style.display = 'block';

  // Auto hide the message if a success
  if (type === 'alert-success') {
    // Use setTimeout to hide the message after the specified duration
    setTimeout(() => {
      status.style.display = 'none';
    }, duration);
  }
};

/**
 * Restores options from local storage and updates the corresponding elements in the options page.
 */
const restoreOptions = (): void => {
  browser.storage.local
    .get(['baseDownloadPath', 'transformPathToLowercase', 'debugMode'])
    .then((items) => {
      const baseDownloadPathInput: HTMLInputElement = document.getElementById('baseDownloadPath') as HTMLInputElement;
      baseDownloadPathInput.value = items.baseDownloadPath;

      const transformPathToLowercaseInput: HTMLInputElement = document.getElementById('transformPathToLowercase') as HTMLInputElement;
      transformPathToLowercaseInput.checked = items.transformPathToLowercase;

      const debugModeInput: HTMLInputElement = document.getElementById('debugMode') as HTMLInputElement;
      debugModeInput.checked = items.debugMode;
    });
};

/**
 * Saves options to local storage based on user input and updates the status accordingly.
 */
const saveOptions = (): void => {
  const baseDownloadPath: string = (document.getElementById('baseDownloadPath') as HTMLInputElement).value;
  const transformPathToLowercase: boolean = (document.getElementById('transformPathToLowercase') as HTMLInputElement).checked;
  const debugMode: boolean = (document.getElementById('debugMode') as HTMLInputElement).checked;

  // Add trailing slash if it doesn't exist
  const normalizedBaseDownloadPath: string = baseDownloadPath.endsWith('/') ? baseDownloadPath : baseDownloadPath + '/';
  
  if(baseDownloadPath.startsWith('/')) {
    flashAlert('Failed to save options. The download path cannot start with a forward slash.', 'alert-danger');
    return;
  }

  // Save options to local storage
  browser.storage.local
    .set({
      baseDownloadPath: normalizedBaseDownloadPath,
      transformPathToLowercase,
      debugMode,
    })
    .then(() => {
      // Update status to let the user know options were saved
      flashAlert('Options saved successfully.', 'alert-success');

      // Reload the options
      restoreOptions();
    })
    .catch((error) => {
      console.error('Error saving options:', error);
      // Update status to inform the user about the error
      flashAlert('Failed to save options. Please try again.', 'alert-danger');
    });
};

document.addEventListener('DOMContentLoaded', restoreOptions);
document.getElementById('save').addEventListener('click', saveOptions);
