import { Tabs } from 'wxt/browser';

/**
 * Class for toggling browser action based on tab URL.
 */
export class BrowserActionToggler {
  
  /**
   * Base URL for the agent interface.
   */
  private static agentBaseUrl: string = 'zendesk.com/agent/tickets';
  
  /**
   * Update browser action based on the tab URL.
   * @param tab The tab object representing the tab whose action needs to be updated.
   */
  public static updateAction(tab: Tabs.Tab): void {
    if (tab.url.includes(BrowserActionToggler.agentBaseUrl)) {
      BrowserActionToggler.enableAction(tab.id);
    }
    else {
      BrowserActionToggler.disableAction(tab.id);
    }
  }

  /**
   * Enable browser action for the given tab.
   * @param tabId The ID of the tab to enable the action for.
   */
  private static enableAction(tabId: number): void {
    browser.action.setIcon({
      tabId: tabId,
      path: '/icon/128.png',
    });
    browser.action.enable(tabId);
  }

  /**
   * Disable browser action for the given tab.
   * @param tabId The ID of the tab to disable the action for.
   */
  private static disableAction(tabId: number): void {
    browser.action.setIcon({
      tabId: tabId,
      path: '/icon/bw128.png',
    });
    browser.action.disable(tabId);
  }
}
