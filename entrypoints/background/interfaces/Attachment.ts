// eslint-disable-next-line @typescript-eslint/no-unused-vars
interface Attachment {
  id: string;
  domain: string;
  title: string;
  organization: string;
  url: string;
  subdomain: string;
}
