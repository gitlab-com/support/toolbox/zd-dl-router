import { AttachmentHelper } from './AttachmentHelper';
import { BaseDownloadButtonManager } from './BaseDownloadButtonManager';

export class ImageDownloadButtonManager extends BaseDownloadButtonManager {
  constructor() {
    super({
      containerClass: 'zdr-img-dl-container',
      selector: 'figure:not([class])',
      processedClass: 'zdr-image-processed',
      buttonTitle: 'Download image',
      iconUrl: browser.runtime.getURL('/icon/128.png'),
      showCount: false
    });
  }

  protected processContainer(container: HTMLElement): void {
    if (container.classList.contains(this.config.processedClass)) {
      const existingButton = container.querySelector('.zdr-img-dl-container');
      if (!existingButton) {
        container.classList.remove(this.config.processedClass);
      } else {
        return;
      }
    }

    if (!container.closest('.zd-comment')) {
      return;
    }

    const image = container.querySelector('img');
    if (!image) {
      return;
    }

    const button = this.createDownloadButton(1);
    button.style.position = 'absolute';
    button.style.top = '8px';
    button.style.right = '8px';

    container.style.position = 'relative';
    
    try {
      container.appendChild(button);
      container.classList.add(this.config.processedClass);
    } catch (error) {
      console.error('Error adding button:', error);
    }
  }

  protected handleDownload(elements: NodeListOf<Element>): void {
    const image = (elements[0].parentElement as Element).querySelector('img');
    if (image) {
      AttachmentHelper.downloadAttachment(image as HTMLImageElement);
    }
  }

  protected getDownloadElementSelector(): string {
    return 'img';
  }
}