import { AttachmentHelper } from './AttachmentHelper';
import { BaseDownloadButtonManager } from './BaseDownloadButtonManager';
import { ThumbnailResolver } from './ThumbnailResolver';

export class AttachmentDownloadButtonManager extends BaseDownloadButtonManager {
  constructor() {
    super({
      containerClass: 'zdr-dl-btn-container',
      selector: 'div[data-test-id="attachment-group-container"], div[data-test-id=attachment-thumbnails-container]',
      processedClass: 'zdr-attachment-group-processed',
      buttonTitle: 'Download all attachments for this comment',
      iconUrl: browser.runtime.getURL('/icon/128.png'),
      showCount: true
    });
  }

  protected processContainer(container: Element): void {
    const attachmentCount = container.querySelectorAll('a, button').length;
    if (attachmentCount > 1) {
      const button = this.createDownloadButton(attachmentCount);
      container.prepend(button);
    }
  }

  protected handleDownload(elements: NodeListOf<Element>): void {
    // If buttons exist, resolve direct src before downloading
    if(Array.from(elements).some(node => node instanceof HTMLButtonElement)) {
      const buttons = Array.from(elements).filter(node => node instanceof HTMLButtonElement);
      const ticketID = window.location.pathname.split('/')[3];
      const thumbnailResolver = new ThumbnailResolver(ticketID);
      
      buttons.forEach(button => {
        thumbnailResolver.resolveDirectSrc(button);
      });
    }

    elements.forEach(element => {
      AttachmentHelper.downloadAttachment(element as HTMLAnchorElement | HTMLButtonElement);
    });
  }

  protected getDownloadElementSelector(): string {
    return 'a, button:not(.zdr-dl-btn)';
  }
}