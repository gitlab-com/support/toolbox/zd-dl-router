import { ButtonConfig } from '../interfaces/ButtonConfig';

export abstract class BaseDownloadButtonManager {
  protected config: ButtonConfig;

  constructor(config: ButtonConfig) {
    this.config = config;
  }

  public observe(): void {
    const observer = new MutationObserver((mutations) => {
      mutations.forEach((mutation) => {
        mutation.addedNodes.forEach((node) => {
          if (node.nodeType === Node.ELEMENT_NODE) {
            const element = node as Element;
            const containers = element.matches(this.config.selector) 
              ? [element] 
              : element.querySelectorAll(this.config.selector);

            containers.forEach(container => {
              if (!container.classList.contains(this.config.processedClass)) {
                container.classList.add(this.config.processedClass);
                this.processContainer(container);
              }
            });
          }
        });
      });
    });

    observer.observe(document.documentElement, {
      childList: true,
      subtree: true
    });
  }

  protected abstract processContainer(container: Element): void;
  protected abstract handleDownload(elements: NodeListOf<Element>): void;

  protected createDownloadButton(count: number): HTMLDivElement {
    const buttonContainer = this.createButtonContainer();
    const button = this.setupDownloadButton(count);
    const image = this.createButtonImage();

    if (this.config.showCount) {
      const countPill = this.createCountPill(count);
      button.appendChild(countPill);
    }
    button.appendChild(image);
    buttonContainer.appendChild(button);
    return buttonContainer;
  }

  private createButtonContainer(): HTMLDivElement {
    const container = document.createElement('div');
    container.className = this.config.containerClass;
    return container;
  }

  private setupDownloadButton(count: number): HTMLButtonElement {
    const button = document.createElement('button');
    button.className = 'zdr-dl-btn';  // Keep the original class name
    button.setAttribute('title', this.config.buttonTitle);
    button.setAttribute('aria-label', `Download all ${count} items`);
    return button;
  }

  private createCountPill(count: number): HTMLDivElement {
    const countPill = document.createElement('div');
    countPill.className = 'zdr-dl-count';
    countPill.textContent = count.toString();
    return countPill;
  }

  private createButtonImage(): HTMLImageElement {
    const image = document.createElement('img');
    image.src = this.config.iconUrl;
    image.style.cssText = 'width:60px;height:60px;';
    return image;
  }

  public bindEventHandler(): void {
    document.addEventListener('click', (event) => {
      const target = event.target as Element;
      const downloadButton = target.closest(`.${this.config.containerClass}`);
      
      if (downloadButton) {
        event.preventDefault();
        const elements = downloadButton.parentElement.querySelectorAll(this.getDownloadElementSelector());
        this.handleDownload(elements);
      }
    });
  }

  protected abstract getDownloadElementSelector(): string;
}