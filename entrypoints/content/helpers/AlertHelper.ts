export class AlertHelper {
  
  /**
   * Displays an alert message to the user and automatically hides it after a specified duration.
   * 
   * The function retrieves an element with the ID 'alert', updates its HTML to include the provided message,
   * and applies a 'visible' class to make it visible. After a delay, it removes the 'visible' class to hide the alert
   * and resets its HTML content to a default message.
   * 
   * @param {string} message - The message to be displayed in the alert. This message is embedded directly
   *                           into the alert element's HTML, so it should be properly sanitized if it includes
   *                           user input to prevent XSS attacks.
   */
  public static flashMessage(message: string) {
    const alert = document.getElementById('alert') as HTMLElement | null;
    if (alert) {
      const messageElement = document.createElement('p');
      messageElement.style.marginRight = '0';
      messageElement.textContent = message;
      
      alert.innerHTML = '';
      alert.appendChild(messageElement);
  
      alert.classList.add('visible');
  
      setTimeout(() => {
        alert.classList.remove('visible');
        alert.innerHTML = '<p><b>Zendesk Alert:</b></p>';
      }, 5000);
    }
  }
}