export class ThumbnailResolver {
  private attachmentsData: Array<ImageMapper>;
  private ticketID: string;

  constructor(ticketID: string) {
    this.attachmentsData = [];
    this.ticketID = ticketID;

    this.makeSyncRequest(`https://${window.location.host}/api/v2/tickets/${this.ticketID}/comments.json`);
  }

  public makeSyncRequest(url: string) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', url, false);
    xhr.send(null);
  
    if (xhr.status === 200) {
      const jsonResponse = JSON.parse(xhr.responseText) as TicketCommentsResponse;
      this.processResponse(jsonResponse);
    } else {
      console.error('Request failed with status:', xhr.status);
    }
  }
  
  public processResponse(response: TicketCommentsResponse) {
    if(response.comments) {
      response.comments.forEach(comment => {
        if (comment['attachments'].length < 1 ) {
          return; // Skip if there are no attachments
        }
        comment['attachments'].forEach(attachment => {
          this.attachmentsData.push({
            'src': attachment['content_url'],
            'thumb_src': attachment['thumbnails'][0] ? attachment['thumbnails'][0]['content_url'] : null,
            'file_name': attachment['file_name'],
            'date': comment['created_at'].slice(0, -1)
          });
        });
      });
    }
  }
  
  public resolveDirectSrc(attachment: HTMLButtonElement) {
    const thumb = attachment.querySelector('img');
    if(thumb) {
      const attachmentMatch = this.attachmentsData.filter(obj => obj.thumb_src === thumb.getAttribute('src'));
      if(attachmentMatch.length > 0) {
        attachment.setAttribute('data-direct-src', attachmentMatch[0].src);
      }
    }

    // If a thumb does not exist, Zendesk has not generated one
    // We can try to find the right link using the file name and date but this isn't perfect
    else {
      let commentDate = attachment.closest('article').querySelector('time').getAttribute('datetime');
      commentDate = commentDate.substring(0, commentDate.lastIndexOf('.'));

      const attachmentMatch = this.attachmentsData.filter(obj => obj.file_name === attachment.getAttribute('title') && obj.date === commentDate);
      if(attachmentMatch.length > 0) {
        attachment.setAttribute('data-direct-src', attachmentMatch[0].src);
      }
    }
  }
}