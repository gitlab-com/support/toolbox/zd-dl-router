export interface ButtonConfig {
  containerClass: string;
  selector: string;
  processedClass: string;
  buttonTitle: string;
  iconUrl: string;
  showCount?: boolean;
}