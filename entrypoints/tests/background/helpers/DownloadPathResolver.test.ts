import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { DownloadPathResolver } from '@/entrypoints/background/helpers/DownloadPathResolver';

vi.mock('wxt/storage', () => ({
  storage: {
    getItem: vi.fn()
  }
}));

describe('DownloadPathResolver', () => {
  const resolver = new DownloadPathResolver();
  let consoleErrorSpy: vi.SpyInstance;

  beforeEach(() => {
    consoleErrorSpy = vi.spyOn(console, 'error').mockImplementation(() => {});
  });
  
  afterEach(() => {
    vi.restoreAllMocks();
  });

  describe('determineRelativeDownloadPath', () => {
    it('should correctly map the ticket ID path', async () => {
      storage.getItem.mockImplementation((key: string) => {
        if (key === 'local:baseDownloadPath') return Promise.resolve('zd-%TICKET_ID%/');
        if (key === 'local:transformPathToLowercase') return Promise.resolve(true);
      });
      
      const path = await resolver.determineRelativeDownloadPath('123', null, null, null);
      expect(path).toBe('zd-123/');
    });

    it('should correctly map the ticket organization and ticket ID path', async () => {
      storage.getItem.mockImplementation((key: string) => {
        if (key === 'local:baseDownloadPath') return Promise.resolve('%TICKET_ORG%/zd-%TICKET_ID%/');
        if (key === 'local:transformPathToLowercase') return Promise.resolve(true);
      });
      
      const path = await resolver.determineRelativeDownloadPath('123', null, 'Acme', null);
      expect(path).toBe('acme/zd-123/');
    });
    
    it('should correctly map the ticket organization and ticket title/ID combo path', async () => {
      storage.getItem.mockImplementation((key: string) => {
        if (key === 'local:baseDownloadPath') return Promise.resolve('%TICKET_ORG%/zd-%TICKET_ID% - %TICKET_TITLE%/');
        if (key === 'local:transformPathToLowercase') return Promise.resolve(true);
      });
      
      const path = await resolver.determineRelativeDownloadPath('123', 'This is my ticket', 'Acme', null);
      expect(path).toBe('acme/zd-123 - this is my ticket/');
    });

    it('should correctly map the ticket subdomain and ticket ID path', async () => {
      storage.getItem.mockImplementation((key: string) => {
        if (key === 'local:baseDownloadPath') return Promise.resolve('%TICKET_SUBDOMAIN%/zd-%TICKET_ID%/');
        if (key === 'local:transformPathToLowercase') return Promise.resolve(true);
      });
      
      const path = await resolver.determineRelativeDownloadPath('123', null, null, 'acme');
      expect(path).toBe('acme/zd-123/');
    });

    it('should return an error if the download path does not start with a forward slash', async () => {
      storage.getItem.mockImplementation((key: string) => {
        if (key === 'local:baseDownloadPath') return Promise.resolve('/zd-%TICKET_ID%/');
        if (key === 'local:transformPathToLowercase') return Promise.resolve(true);
      });
      
      await resolver.determineRelativeDownloadPath('123', 'Test Ticket', 'Acme Inc.', 'acme');

      expect(consoleErrorSpy).toHaveBeenCalledWith(
        'The download path cannot start with a forward slash. Check the extension settings.'
      );
    });

    it('should not return an error if the download path does not start with a forward slash', async () => {
      storage.getItem.mockImplementation((key: string) => {
        if (key === 'local:baseDownloadPath') return Promise.resolve('zd-%TICKET_ID%/');
        if (key === 'local:transformPathToLowercase') return Promise.resolve(true);
      });
      
      await resolver.determineRelativeDownloadPath('123', 'Test Ticket', 'Acme Inc.', 'acme');

      expect(consoleErrorSpy).not.toHaveBeenCalled();
    });
  });

  describe('transformToSafeFolderName', () => {
    it('should remove special characters and leave case as is', () => {
      const safeName = resolver['transformToSafeFolderName']('Example & Name', false);
      expect(safeName).toBe('Example Name');
    });

    it('should remove special characters and convert to lowercase', () => {
      const safeName = resolver['transformToSafeFolderName']('Example & Name', true);
      expect(safeName).toBe('example name');
    });

    it('should handle empty strings without errors', () => {
      const safeName = resolver['transformToSafeFolderName']('', true);
      expect(safeName).toBe('');
    });
  });
});