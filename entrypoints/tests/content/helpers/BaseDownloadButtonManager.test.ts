// @vitest-environment happy-dom
import { describe, beforeEach, afterEach, it, expect, vi } from 'vitest';
import { BaseDownloadButtonManager } from '@/entrypoints/content/helpers/BaseDownloadButtonManager';
import { ButtonConfig } from '@/entrypoints/content/interfaces/ButtonConfig';

// Create a concrete implementation for testing
class TestDownloadButtonManager extends BaseDownloadButtonManager {
  public downloadCalled = false;
  public processedContainers: Element[] = [];

  protected processContainer(container: Element): void {
    this.processedContainers.push(container);
    const count = 5; // Mock count for testing
    const downloadButton = this.createDownloadButton(count);
    container.appendChild(downloadButton);
  }

  protected handleDownload(_elements: NodeListOf<Element>): void {
    this.downloadCalled = true;
  }

  protected getDownloadElementSelector(): string {
    return '.download-item';
  }
}

describe('BaseDownloadButtonManager', () => {
  let manager: TestDownloadButtonManager;
  let config: ButtonConfig;
  let mockObserverCallback: MutationCallback;

  // Helper function to create a NodeList-like object
  const createNodeList = (nodes: Node[]): NodeList => {
    const nodeList = {
      length: nodes.length,
      item(index: number) {
        return nodes[index];
      },
      [Symbol.iterator]() {
        return nodes[Symbol.iterator]();
      },
      forEach(callback: (node: Node, index: number, list: NodeList) => void) {
        nodes.forEach((node, index) => callback(node, index, this));
      },
      entries() {
        return nodes.entries();
      },
      keys() {
        return nodes.keys();
      },
      values() {
        return nodes.values();
      }
    };
    nodes.forEach((node, index) => {
      Object.defineProperty(nodeList, index, {
        get: () => nodes[index],
        enumerable: true
      });
    });
    return nodeList as NodeList;
  };

  beforeEach(() => {
    // Setup test configuration
    config = {
      selector: '.container',
      processedClass: 'processed',
      containerClass: 'download-container',
      buttonTitle: 'Download All',
      iconUrl: 'test-icon.png',
      showCount: true
    };

    // Mock MutationObserver with callback capture
    vi.stubGlobal('MutationObserver', class {
      constructor(callback: MutationCallback) {
        mockObserverCallback = callback;
      }
      observe() {}
      disconnect() {}
    });

    manager = new TestDownloadButtonManager(config);
  });

  afterEach(() => {
    vi.unstubAllGlobals();
    document.body.innerHTML = '';
  });

  describe('constructor', () => {
    it('should initialize with the provided config', () => {
      expect(manager['config']).toBe(config);
    });
  });

  describe('createDownloadButton', () => {
    let button: HTMLDivElement;

    beforeEach(() => {
      button = manager['createDownloadButton'](5);
    });

    it('should create a button container with correct class', () => {
      expect(button.className).toBe(config.containerClass);
    });

    it('should create a button with correct attributes', () => {
      const downloadButton = button.querySelector('button');
      expect(downloadButton).toBeTruthy();
      expect(downloadButton.className).toBe('zdr-dl-btn');
      expect(downloadButton.getAttribute('title')).toBe(config.buttonTitle);
      expect(downloadButton.getAttribute('aria-label')).toBe('Download all 5 items');
    });

    it('should create count pill when showCount is true', () => {
      const countPill = button.querySelector('.zdr-dl-count');
      expect(countPill).toBeTruthy();
      expect(countPill.textContent).toBe('5');
    });

    it('should not create count pill when showCount is false', () => {
      config.showCount = false;
      manager = new TestDownloadButtonManager(config);
      button = manager['createDownloadButton'](5);
      
      const countPill = button.querySelector('.zdr-dl-count');
      expect(countPill).toBeNull();
    });

    it('should create button image with correct src', () => {
      const image = button.querySelector('img');
      expect(image).toBeTruthy();
      expect(image.src.endsWith('/test-icon.png')).toBeTruthy();
    });
  });

  describe('observe', () => {
    let mockObserverInstance: { 
      observe: (target: Node, options: MutationObserverInit) => void 
    };
    
    beforeEach(() => {
      mockObserverInstance = { observe: vi.fn() };
      vi.stubGlobal('MutationObserver', vi.fn((callback) => {
        mockObserverCallback = callback;
        return mockObserverInstance;
      }));
    });

    it('should initialize MutationObserver with correct config', () => {
      manager.observe();
      
      expect(mockObserverInstance.observe).toHaveBeenCalledWith(
        document.documentElement,
        {
          childList: true,
          subtree: true
        }
      );
    });

    it('should process new matching elements', () => {
      manager.observe();

      // Create test container
      const container = document.createElement('div');
      container.className = 'container';
      document.body.appendChild(container);

      // Create a proper MutationRecord
      const mutation: MutationRecord = {
        addedNodes: createNodeList([container]),
        type: 'childList',
        target: document.body,
        attributeName: null,
        attributeNamespace: null,
        nextSibling: null,
        previousSibling: null,
        oldValue: null,
        removedNodes: createNodeList([])
      };

      // Call the callback with proper parameters
      mockObserverCallback([mutation], null as unknown as MutationObserver);

      expect(container.classList.contains(config.processedClass)).toBeTruthy();
      expect(manager.processedContainers).toContain(container);
    });
  });

  describe('bindEventHandler', () => {
    it('should handle click events on download buttons', () => {
      manager.bindEventHandler();

      // Create and append a download button
      const container = document.createElement('div');
      const downloadButton = manager['createDownloadButton'](5);
      container.appendChild(downloadButton);
      document.body.appendChild(container);

      // Create some download items
      const downloadItem = document.createElement('div');
      downloadItem.className = 'download-item';
      container.appendChild(downloadItem);

      // Simulate click
      downloadButton.querySelector('button')?.click();

      expect(manager.downloadCalled).toBeTruthy();
    });

    it('should not trigger download for non-download button clicks', () => {
      manager.bindEventHandler();

      const regularButton = document.createElement('button');
      document.body.appendChild(regularButton);
      regularButton.click();

      expect(manager.downloadCalled).toBeFalsy();
    });
  });

  describe('DOM mutation handling', () => {
    it('should process nested matching elements', () => {
      manager.observe();

      const outerDiv = document.createElement('div');
      const container = document.createElement('div');
      container.className = 'container';
      outerDiv.appendChild(container);
      document.body.appendChild(outerDiv);

      // Create a proper MutationRecord
      const mutation: MutationRecord = {
        addedNodes: createNodeList([outerDiv]),
        type: 'childList',
        target: document.body,
        attributeName: null,
        attributeNamespace: null,
        nextSibling: null,
        previousSibling: null,
        oldValue: null,
        removedNodes: createNodeList([])
      };

      mockObserverCallback([mutation], null as unknown as MutationObserver);

      expect(container.classList.contains(config.processedClass)).toBeTruthy();
      expect(manager.processedContainers).toContain(container);
    });

    it('should not process already processed elements', () => {
      manager.observe();

      const container = document.createElement('div');
      container.className = 'container';
      container.classList.add(config.processedClass);
      document.body.appendChild(container);

      // Create a proper MutationRecord
      const mutation: MutationRecord = {
        addedNodes: createNodeList([container]),
        type: 'childList',
        target: document.body,
        attributeName: null,
        attributeNamespace: null,
        nextSibling: null,
        previousSibling: null,
        oldValue: null,
        removedNodes: createNodeList([])
      };

      mockObserverCallback([mutation], null as unknown as MutationObserver);

      expect(manager.processedContainers).not.toContain(container);
    });
  });
});