// @vitest-environment happy-dom
import { describe, beforeEach, afterEach, it, expect, vi } from 'vitest';
import { ImageDownloadButtonManager } from '@/entrypoints/content/helpers/ImageDownloadButtonManager';
import { AttachmentHelper } from '@/entrypoints/content/helpers/AttachmentHelper';

// Mock browser.runtime.getURL
vi.stubGlobal('browser', {
  runtime: {
    getURL: vi.fn((path) => `chrome-extension://test-extension-id${path}`)
  }
});

describe('ImageDownloadButtonManager', () => {
  let manager: ImageDownloadButtonManager;

  beforeEach(() => {
    // Mock MutationObserver (needed for constructor)
    vi.stubGlobal('MutationObserver', class {
      constructor() {}
      observe() {}
      disconnect() {}
    });

    manager = new ImageDownloadButtonManager();
    document.body.innerHTML = '';
  });

  afterEach(() => {
    vi.clearAllMocks();
    vi.unstubAllGlobals();
  });

  describe('constructor', () => {
    it('should initialize with correct config values', () => {
      expect(manager['config']).toEqual({
        containerClass: 'zdr-img-dl-container',
        selector: 'figure:not([class])',
        processedClass: 'zdr-image-processed',
        buttonTitle: 'Download image',
        iconUrl: 'chrome-extension://test-extension-id/icon/128.png',
        showCount: false
      });
    });
  });

  describe('processContainer', () => {
    it('should position button correctly for valid image in comment', () => {
      const comment = document.createElement('div');
      comment.className = 'zd-comment';
      
      const container = document.createElement('figure') as HTMLElement;
      const image = document.createElement('img');
      image.src = 'test.jpg';
      container.appendChild(image);
      comment.appendChild(container);
      document.body.appendChild(comment);

      manager['processContainer'](container);

      const button = container.querySelector('.zdr-img-dl-container') as HTMLElement;
      expect(button).toBeTruthy();
      expect(button.style.position).toBe('absolute');
      expect(button.style.top).toBe('8px');
      expect(button.style.right).toBe('8px');
      expect(container.style.position).toBe('relative');
    });

    it('should handle missing button with processed class', () => {
      const comment = document.createElement('div');
      comment.className = 'zd-comment';

      const container = document.createElement('figure') as HTMLElement;
      container.classList.add('zdr-image-processed');
      const image = document.createElement('img');
      container.appendChild(image);
      comment.appendChild(container);
      document.body.appendChild(comment);

      manager['processContainer'](container);

      // Should have removed and re-added the class
      expect(container.classList.contains('zdr-image-processed')).toBeTruthy();
      expect(container.querySelector('.zdr-img-dl-container')).toBeTruthy();
    });

    describe('should skip processing when', () => {
      it('container is not in a comment', () => {
        const container = document.createElement('figure') as HTMLElement;
        const image = document.createElement('img');
        container.appendChild(image);
        document.body.appendChild(container);

        manager['processContainer'](container);
        expect(container.querySelector('.zdr-img-dl-container')).toBeNull();
      });

      it('no image is present', () => {
        const comment = document.createElement('div');
        comment.className = 'zd-comment';
        
        const container = document.createElement('figure') as HTMLElement;
        comment.appendChild(container);
        document.body.appendChild(comment);

        manager['processContainer'](container);
        expect(container.querySelector('.zdr-img-dl-container')).toBeNull();
      });

      it('button already exists', () => {
        const comment = document.createElement('div');
        comment.className = 'zd-comment';
        
        const container = document.createElement('figure') as HTMLElement;
        container.classList.add('zdr-image-processed'); // Add this back
        const image = document.createElement('img');
        container.appendChild(image);

        const existingButton = document.createElement('div');
        existingButton.className = 'zdr-img-dl-container';
        container.appendChild(existingButton);

        comment.appendChild(container);
        document.body.appendChild(comment);

        manager['processContainer'](container);
        expect(container.querySelectorAll('.zdr-img-dl-container').length).toBe(1);
      });
    });
  });

  describe('handleDownload', () => {
    beforeEach(() => {
      vi.spyOn(AttachmentHelper, 'downloadAttachment').mockImplementation(() => {});
    });

    it('should download the image from parent container', () => {
      const container = document.createElement('figure');
      const image = document.createElement('img');
      image.src = 'test.jpg';
      container.appendChild(image);

      const elements = container.querySelectorAll('img');
      manager['handleDownload'](elements);

      expect(AttachmentHelper.downloadAttachment).toHaveBeenCalledWith(image);
    });
  });

  describe('getDownloadElementSelector', () => {
    it('should return img selector', () => {
      expect(manager['getDownloadElementSelector']()).toBe('img');
    });
  });
});