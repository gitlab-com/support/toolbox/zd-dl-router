// @vitest-environment happy-dom
import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { AlertHelper } from '@/entrypoints/content/helpers/AlertHelper';

describe('AlertHelper', () => {
  let alertElement: HTMLElement;

  beforeEach(() => {
    vi.useFakeTimers();
    
    document.body.innerHTML = `
      <div id="alert">
        <p><b>Zendesk Alert:</b></p>
      </div>
    `;

    alertElement = document.getElementById('alert')!;
  });

  afterEach(() => {
    vi.useRealTimers();
    document.body.innerHTML = '';
  });

  it('should update alert HTML with message', () => {
    AlertHelper.flashMessage('Test message');
    
    expect(alertElement.innerHTML).toEqual('<p style="margin-right: 0px;">Test message</p>');
  });

  it('should add visible class to alert', () => {
    AlertHelper.flashMessage('Test message');

    expect(alertElement.classList.contains('visible')).toBeTruthy();
  });

  it('should remove visible class after delay', async () => {
    AlertHelper.flashMessage('Test message');

    vi.advanceTimersByTime(5100);

    expect(alertElement.classList.contains('visible')).toBeFalsy();
  });

  it('should reset alert HTML after delay', async () => {
    AlertHelper.flashMessage('Test message');

    vi.advanceTimersByTime(5100);

    expect(alertElement.innerHTML).toEqual('<p><b>Zendesk Alert:</b></p>');
  });
});