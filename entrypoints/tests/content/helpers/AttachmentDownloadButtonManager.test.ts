// @vitest-environment happy-dom
import { describe, beforeEach, afterEach, it, expect, vi } from 'vitest';
import { AttachmentDownloadButtonManager } from '@/entrypoints/content/helpers/AttachmentDownloadButtonManager';
import { AttachmentHelper } from '@/entrypoints/content/helpers/AttachmentHelper';
import { ThumbnailResolver } from '@/entrypoints/content/helpers/ThumbnailResolver';

// Mock browser.runtime.getURL
vi.stubGlobal('browser', {
  runtime: {
    getURL: vi.fn((path) => `chrome-extension://test-extension-id${path}`)
  }
});

describe('AttachmentDownloadButtonManager', () => {
  let manager: AttachmentDownloadButtonManager;

  beforeEach(() => {
    // Mock MutationObserver (needed for constructor)
    vi.stubGlobal('MutationObserver', class {
      constructor() {}
      observe() {}
      disconnect() {}
    });

    // Mock window.location (needed for constructor)
    Object.defineProperty(window, 'location', {
      value: {
        pathname: '/agent/tickets/123456'
      },
      writable: true
    });

    manager = new AttachmentDownloadButtonManager();
    document.body.innerHTML = '';
  });

  afterEach(() => {
    vi.clearAllMocks();
    vi.unstubAllGlobals();
  });

  describe('constructor', () => {
    it('should initialize with correct config values', () => {
      expect(manager['config']).toEqual({
        containerClass: 'zdr-dl-btn-container',
        selector: 'div[data-test-id="attachment-group-container"], div[data-test-id=attachment-thumbnails-container]',
        processedClass: 'zdr-attachment-group-processed',
        buttonTitle: 'Download all attachments for this comment',
        iconUrl: 'chrome-extension://test-extension-id/icon/128.png',
        showCount: true
      });
    });
  });

  describe('processContainer', () => {
    // This tests the unique multiple attachments behavior
    it('should add download button only when container has multiple attachments', () => {
      const container = document.createElement('div');
      container.innerHTML = `
        <a href="/attachment1.pdf">Attachment 1</a>
        <a href="/attachment2.pdf">Attachment 2</a>
        <button>Attachment 3</button>
      `;

      manager['processContainer'](container);
      expect(container.querySelector('.zdr-dl-btn-container')).toBeTruthy();

      // Test single attachment case
      const singleContainer = document.createElement('div');
      singleContainer.innerHTML = '<a href="/attachment1.pdf">Attachment 1</a>';
      
      manager['processContainer'](singleContainer);
      expect(singleContainer.querySelector('.zdr-dl-btn-container')).toBeNull();
    });
  });

  describe('handleDownload', () => {
    beforeEach(() => {
      vi.spyOn(AttachmentHelper, 'downloadAttachment').mockImplementation(() => {});
      vi.spyOn(ThumbnailResolver.prototype, 'resolveDirectSrc').mockImplementation(() => {});
    });

    it('should handle mixed attachment types and resolve thumbnails appropriately', () => {
      const container = document.createElement('div');
      container.innerHTML = `
        <a href="/attachment1.pdf">Attachment 1</a>
        <button class="thumbnail">Attachment 2</button>
        <a href="/attachment3.pdf">Attachment 3</a>
      `;
      const elements = container.querySelectorAll('a, button');

      manager['handleDownload'](elements);

      expect(ThumbnailResolver.prototype.resolveDirectSrc).toHaveBeenCalledTimes(1);
      expect(AttachmentHelper.downloadAttachment).toHaveBeenCalledTimes(3);
    });

    it('should handle link attachments without resolving thumbnails', () => {
      const container = document.createElement('div');
      container.innerHTML = `
        <a href="/attachment1.pdf">Attachment 1</a>
        <a href="/attachment2.pdf">Attachment 2</a>
      `;
      const elements = container.querySelectorAll('a');

      manager['handleDownload'](elements);

      expect(ThumbnailResolver.prototype.resolveDirectSrc).not.toHaveBeenCalled();
      expect(AttachmentHelper.downloadAttachment).toHaveBeenCalledTimes(2);
    });
  });

  describe('getDownloadElementSelector', () => {
    it('should return correct selector for attachments', () => {
      expect(manager['getDownloadElementSelector']()).toBe('a, button:not(.zdr-dl-btn)');
    });
  });
});