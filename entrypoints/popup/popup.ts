import { DebugLogger } from '@/components/helpers/DebugLogger';
import './style.scss';

// const restoreOptions = () => {
//   browser.storage.local.get(['routing']).then((s) => {
//     (document.getElementById('routing') as HTMLInputElement).value = s.routing;
//   });
// };

const setDarkMode = () => {
  if (
    window.matchMedia &&
    window.matchMedia('(prefers-color-scheme: dark)').matches
  ) {
    document.body.setAttribute('data-bs-theme', 'dark');
  }
};

document.addEventListener('DOMContentLoaded', async () => {
  console.info('Popup script initialised');

  setDarkMode();
  // restoreOptions();

  const openDownloadFolder = document.getElementById('open-download-folder');
  openDownloadFolder.addEventListener('click', () => {
    DebugLogger.debugMessage('ACTION: Open download folder');
    browser.runtime.sendMessage({action: 'open-download-folder'});
  });

  const downloadAllAttachments = document.getElementById('download-all-attachments');
  downloadAllAttachments.addEventListener('click', () => {
    DebugLogger.debugMessage('ACTION: Download all attachments');

    browser.tabs.query({active: true, currentWindow: true}).then((tabs) => {
      browser.tabs.sendMessage(tabs[0].id, {action: 'download-all-attachments'});
    });
  });

  const options = document.getElementById('options');
  options.addEventListener('click', () => {
    DebugLogger.debugMessage('ACTION: Open extension options');
    browser.runtime.openOptionsPage();
  });

  const routing = document.getElementById('routing') as HTMLInputElement;
  routing.addEventListener('change', () => {
    DebugLogger.debugMessage('ACTION: Changing routing options');
    browser.storage.local.set({
      routing: (document.getElementById('routing') as HTMLInputElement).value,
    });
  });
});