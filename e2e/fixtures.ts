import { test as base, chromium, type BrowserContext } from '@playwright/test';
import savedSessionState from '../.playwright/auth/user.json' assert { type: 'json' };
import path from 'path';

const pathToExtension = path.resolve('.output/chrome-mv3');

export const test = base.extend<{
  context: BrowserContext;
  extensionId: string;
}>({
  context: async ({}, use) => {
    const context = await chromium.launchPersistentContext('', {
      headless: false,
      args: [
        `--disable-extensions-except=${pathToExtension}`,
        `--load-extension=${pathToExtension}`,
      ],
    });

    /*
     * WORKAROUND - Persistent sessions don't automatically pickup the authentication from
     * auth.setup.ts
     * This generates the "ExperimentalWarning: Importing JSON modules is an experimental feature" message
     * https://github.com/microsoft/playwright/issues/14949#issuecomment-1690402951
     */
    await context.addCookies(savedSessionState.cookies);

    await use(context);
    await context.close();
  },
  extensionId: async ({ context }, use) => {
    let background: { url(): string };
    if (pathToExtension.endsWith('-mv3')) {
      [background] = context.serviceWorkers();
      if (!background) background = await context.waitForEvent('serviceworker');
    } else {
      [background] = context.backgroundPages();
      if (!background)
        background = await context.waitForEvent('backgroundpage');
    }

    const extensionId = background.url().split('/')[2];
    await use(extensionId);
  },
});
export const expect = test.expect;